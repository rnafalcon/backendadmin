declare namespace Express {
  export interface Request {
    loggedUserId: number;
  }
}
