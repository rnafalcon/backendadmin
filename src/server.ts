import 'reflect-metadata';
import 'dotenv/config';

import express from 'express';
import 'express-async-errors';
import cors from 'cors';

import '@config/dependencyContainer';
import './database/typeorm';
import errorHandler from '@middlewares/errorHandler';
import routes from './routes';
import log from '@config/log4js';

const app = express();
app.use(cors());
app.use(express.json());
app.use(routes);

app.use(errorHandler);

app.listen(process.env.APP_PORT, () => {
  log.info(`🏞  Server FalconAdmin started on port ${process.env.APP_PORT}`);
});
