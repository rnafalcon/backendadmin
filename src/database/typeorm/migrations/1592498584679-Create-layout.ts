import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateLayout1592498584679 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`
      CREATE TABLE layout (
        id_layout SERIAL PRIMARY KEY,
        id_tipo_documento SMALLINT NOT NULL,
        id_municipio SMALLINT NOT NULL,
        created TIMESTAMP NOT NULL DEFAULT now(),
        updated TIMESTAMP NOT NULL DEFAULT now()
      )
    `);

    await queryRunner.query(`
      ALTER TABLE layout
        ADD CONSTRAINT fk_layout_id_tipo_documento
        FOREIGN KEY (id_tipo_documento) REFERENCES tipo_documento(id_tipo_documento)
        ON DELETE NO ACTION ON UPDATE NO ACTION
    `);

    await queryRunner.query(`
      ALTER TABLE layout
        ADD CONSTRAINT fk_layout_id_municipio
        FOREIGN KEY (id_municipio) REFERENCES municipio(id_municipio)
        ON DELETE NO ACTION ON UPDATE NO ACTION
    `);

    await queryRunner.query(`
      CREATE UNIQUE INDEX uidx_id_tipo_documento_id_municipio
        ON layout (id_tipo_documento, id_municipio)
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.dropTable('layout');
  }
}
