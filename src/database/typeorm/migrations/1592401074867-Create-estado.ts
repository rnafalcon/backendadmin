import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class CreateEstado1592401074867 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`
      CREATE TABLE estado (
        id_estado SMALLSERIAL PRIMARY KEY,
        nome VARCHAR(60) NOT NULL,
        sigla VARCHAR(2) NOT NULL,
        codigo_uf SMALLINT NOT NULL,
        created TIMESTAMP NOT NULL DEFAULT NOW(),
        updated TIMESTAMP NOT NULL DEFAULT NOW()
      )
    `);

    await queryRunner.query(`
      INSERT INTO estado (nome, sigla, codigo_uf)
        VALUES
        ('Acre', 'AC', 12),
        ('Alagoas', 'AL', 27),
        ('Amapá', 'AP', 16),
        ('Amazonas', 'AM', 13),
        ('Bahia', 'BA', 29),
        ('Ceará', 'CE', 23),
        ('Distrito Federal', 'DF', 53),
        ('Espírito Santo', 'ES', 32),
        ('Goiás', 'GO', 52),
        ('Maranhão', 'MA', 21),
        ('Mato Grosso do Sul', 'MS', 50),
        ('Mato Grosso', 'MT', 51),
        ('Minas Gerais', 'MG', 31),
        ('Paraná', 'PR', 41),
        ('Paraíba', 'PB', 25),
        ('Pará', 'PA', 15),
        ('Pernambuco', 'PE', 26),
        ('Piauí', 'PI', 22),
        ('Rio de Janeiro', 'RJ', 33),
        ('Rio Grande do Norte', 'RN', 24),
        ('Rio Grande do Sul', 'RS', 43),
        ('Rondônia', 'RO', 11),
        ('Roraima', 'RR', 14),
        ('Santa Catarina', 'SC', 42),
        ('Sergipe', 'SE', 28),
        ('São Paulo', 'SP', 35),
        ('Tocantins', 'TO', 17)
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.dropTable('estado');
  }
}
