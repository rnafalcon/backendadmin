import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateEmpresa1592485883599 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`
      CREATE TABLE empresa (
        id_empresa BIGSERIAL PRIMARY KEY,
        razao_social VARCHAR(200) NOT NULL,
        telefone VARCHAR(50) NOT NULL,
        email VARCHAR(100),
        situacao VARCHAR(30),
        bairro VARCHAR(100),
        logradouro VARCHAR(200),
        complemento VARCHAR(100),
        numero VARCHAR(10),
        cep VARCHAR(10) NOT NULL,
        porte VARCHAR(60),
        abertura DATE NOT NULL,
        natureza_juridica VARCHAR(100) NOT NULL,
        nome_fantasia VARCHAR(200) NOT NULL,
        cnpj VARCHAR(18) NOT NULL,
        tipo VARCHAR(15) NOT NULL,
        active BOOLEAN NOT NULL DEFAULT TRUE,
        id_municipio SMALLINT NOT NULL,
        created TIMESTAMP NOT NULL DEFAULT NOW(),
        updated TIMESTAMP NOT NULL DEFAULT NOW()
      )
  `);

    await queryRunner.query(`
      ALTER TABLE empresa
        ADD CONSTRAINT fk_empresa_id_municipio
        FOREIGN KEY (id_municipio) REFERENCES municipio (id_municipio)
        ON DELETE NO ACTION ON UPDATE NO ACTION
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.dropTable('empresa');
  }
}
