import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateTipoDocumento1592490110592 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`
      CREATE TABLE tipo_documento (
        id_tipo_documento SMALLINT PRIMARY KEY,
        nome VARCHAR(60) NOT NULL,
        sigla VARCHAR(10) NOT NULL,
        created TIMESTAMP NOT NULL DEFAULT now(),
        updated TIMESTAMP NOT NULL DEFAULT now()
      )
    `);

    await queryRunner.query(`
      ALTER TABLE tipo_documento ADD CONSTRAINT "uq_tipo_documento_sigla" UNIQUE (sigla);
    `);

    await queryRunner.query(`
      INSERT INTO tipo_documento (id_tipo_documento, nome, sigla)
        VALUES
        (1, 'Nota Fiscal Eletrônica', 'NF-e'),
        (2, 'Nota Fiscal de Serviço Eletrônica', 'NFS-e');
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.dropTable('tipo_documento');
  }
}
