import { MigrationInterface, QueryRunner } from 'typeorm';

export class InsertCampoLayoutNfseCuritiba1592919849744 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    const layout = await queryRunner.query(`
      SELECT id_layout FROM layout l
      WHERE l.id_tipo_documento = (
                  SELECT id_tipo_documento
                  FROM tipo_documento
                  WHERE sigla = 'NFS-e'
                ) AND
            l.id_municipio = (
                  SELECT id_municipio
                  FROM municipio
                  WHERE codigo_ibge_municipio = 4106902
                );
      `);

    const { id_layout } = layout[0];

    await queryRunner.query(`
      INSERT INTO campo_layout (id_layout, id_campo, id_campo_pai, id_tipo_campo, ordem, obrigatorio, tamanho, "decimal", json_key)
      VALUES
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'Nfse'), null, (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'c'), 1, true, null, null, 'nfse'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'InfNfse'), (SELECT id_campo FROM campo c WHERE nome = 'Nfse'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'c'), 1, true, null, null, 'infNfse'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'Numero'), (SELECT id_campo FROM campo c WHERE nome = 'InfNfse'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'N'), 1, true, 15, 0, 'numero'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'CodigoVerificacao'), (SELECT id_campo FROM campo c WHERE nome = 'InfNfse'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'C'), 2, true, 9, null, 'codigoVerificacao'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'DataEmissao'), (SELECT id_campo FROM campo c WHERE nome = 'InfNfse'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'DH'), 3, true, null, null, 'dataEmissao'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'IdentificacaoRps'), (SELECT id_campo FROM campo c WHERE nome = 'InfNfse'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'c'), 4, false, null, null, 'identificacaoRps'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'Numero'), (SELECT id_campo FROM campo c WHERE nome = 'IdentificacaoRps'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'N'), 1, true, 15, 0, 'numero'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'Serie'), (SELECT id_campo FROM campo c WHERE nome = 'IdentificacaoRps'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'C'), 2, true, 5, null, 'serie'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'Tipo'), (SELECT id_campo FROM campo c WHERE nome = 'IdentificacaoRps'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'N'), 3, true, 1, 0, 'tipo'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'DataEmissaoRps'), (SELECT id_campo FROM campo c WHERE nome = 'InfNfse'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'D'), 5, false, null, null, 'dataEmissaoRps'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'NaturezaOperacao'), (SELECT id_campo FROM campo c WHERE nome = 'InfNfse'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'N'), 6, true, 2, 0, 'naturezaOperacao'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'RegimeEspecialTributacao'), (SELECT id_campo FROM campo c WHERE nome = 'InfNfse'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'N'), 7, false, 2, 0, 'regimeEspecialTributacao'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'OptanteSimplesNacional'), (SELECT id_campo FROM campo c WHERE nome = 'InfNfse'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'N'), 8, true, 1, 0, 'optanteSimplesNacional'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'IncentivadorCultural'), (SELECT id_campo FROM campo c WHERE nome = 'InfNfse'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'N'), 9, true, 1, 0, 'incentivadorCultural'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'Competencia'), (SELECT id_campo FROM campo c WHERE nome = 'InfNfse'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'DH'), 10, true, null, null, 'competencia'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'NfseSubstituida'), (SELECT id_campo FROM campo c WHERE nome = 'InfNfse'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'N'), 11, false, 15, 0, 'nfseSubstituida'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'OutrasInformacoes'), (SELECT id_campo FROM campo c WHERE nome = 'InfNfse'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'C'), 12, false, 255, null, 'outrasInformacoes'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'Servico'), (SELECT id_campo FROM campo c WHERE nome = 'InfNfse'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'c'), 13, true, null, null, 'servico'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'Valores'), (SELECT id_campo FROM campo c WHERE nome = 'Servico'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'c'), 1, true, null, null, 'valores'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'ValorServicos'), (SELECT id_campo FROM campo c WHERE nome = 'Valores'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'N'), 1, true, 15, 2, 'valorServicos'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'ValorDeducoes'), (SELECT id_campo FROM campo c WHERE nome = 'Valores'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'N'), 2, false, 15, 2, 'valorDeducoes'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'NumeroDeducao'), (SELECT id_campo FROM campo c WHERE nome = 'Valores'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'N'), 3, false, 19, 0, 'numeroDeducao'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'ValorPis'), (SELECT id_campo FROM campo c WHERE nome = 'Valores'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'N'), 4, false, 15, 2, 'valorPis'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'ValorCofins'), (SELECT id_campo FROM campo c WHERE nome = 'Valores'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'N'), 5, false, 15, 2, 'valorCofins'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'ValorInss'), (SELECT id_campo FROM campo c WHERE nome = 'Valores'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'N'), 6, false, 15, 2, 'valorInss'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'ValorIr'), (SELECT id_campo FROM campo c WHERE nome = 'Valores'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'N'), 7, false, 15, 2, 'valorIr'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'ValorCsll'), (SELECT id_campo FROM campo c WHERE nome = 'Valores'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'N'), 8, false, 15, 2, 'valorCsll'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'IssRetido'), (SELECT id_campo FROM campo c WHERE nome = 'Valores'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'N'), 9, true, 1, 0, 'issRetido'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'ValorIss'), (SELECT id_campo FROM campo c WHERE nome = 'Valores'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'N'), 10, false, 15, 2, 'valorIss'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'OutrasRetencoes'), (SELECT id_campo FROM campo c WHERE nome = 'Valores'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'N'), 11, false, 15, 2, 'outrasRetencoes'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'BaseCalculo'), (SELECT id_campo FROM campo c WHERE nome = 'Valores'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'N'), 12, true, 15, 2, 'baseCalculo'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'Aliquota'), (SELECT id_campo FROM campo c WHERE nome = 'Valores'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'N'), 13, false, 5, 4, 'aliquota'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'ValorLiquidoNfse'), (SELECT id_campo FROM campo c WHERE nome = 'Valores'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'N'), 14, false, 15, 2, 'valorLiquidoNfse'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'ValorIssRetido'), (SELECT id_campo FROM campo c WHERE nome = 'Valores'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'N'), 15, false, 15, 2, 'valorIssRetido'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'DescontoIncondicionado'), (SELECT id_campo FROM campo c WHERE nome = 'Valores'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'N'), 16, false, 15, 2, 'descontoIncondicionado'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'DescontoCondicionado'), (SELECT id_campo FROM campo c WHERE nome = 'Valores'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'N'), 17, false, 15, 2, 'descontoCondicionado'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'ItemListaServico'), (SELECT id_campo FROM campo c WHERE nome = 'Servico'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'C'), 2, true, 5, null, 'itemListaServico'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'CodigoCnae'), (SELECT id_campo FROM campo c WHERE nome = 'Servico'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'N'), 3, false, 7, 0, 'codigoCnae'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'CodigoTributacaoMunicipio'), (SELECT id_campo FROM campo c WHERE nome = 'Servico'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'C'), 4, false, 20, null, 'codigoTributacaoMunicipio'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'Discriminacao'), (SELECT id_campo FROM campo c WHERE nome = 'Servico'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'C'), 5, true, 2000, null, 'discriminacao'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'CodigoMunicipio'), (SELECT id_campo FROM campo c WHERE nome = 'Servico'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'N'), 6, true, 7, 0, 'codigoMunicipio'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'ValorCredito'), (SELECT id_campo FROM campo c WHERE nome = 'InfNfse'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'N'), 14, false, 15, 2, 'valorCredito'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'PrestadorServico'), (SELECT id_campo FROM campo c WHERE nome = 'InfNfse'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'c'), 15, true, null, null, 'prestadorServico'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'IdentificacaoPrestador'), (SELECT id_campo FROM campo c WHERE nome = 'PrestadorServico'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'c'), 1, true, null, null, 'identificacaoPrestador'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'Cnpj'), (SELECT id_campo FROM campo c WHERE nome = 'IdentificacaoPrestador'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'C'), 1, true, 14, null, 'cnpj'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'InscricaoMunicipal'), (SELECT id_campo FROM campo c WHERE nome = 'IdentificacaoPrestador'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'C'), 2, false, 15, null, 'inscricaoMunicipal'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'RazaoSocial'), (SELECT id_campo FROM campo c WHERE nome = 'PrestadorServico'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'C'), 2, true, 115, null, 'razaoSocial'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'NomeFantasia'), (SELECT id_campo FROM campo c WHERE nome = 'PrestadorServico'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'C'), 3, false, 60, null, 'nomeFantasia'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'Endereco'), (SELECT id_campo FROM campo c WHERE nome = 'PrestadorServico'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'c'), 4, true, null, null, 'endereco'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'Endereco'), (SELECT id_campo FROM campo c WHERE nome = 'Endereco'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'C'), 1, false, 125, null, 'endereco'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'Numero'), (SELECT id_campo FROM campo c WHERE nome = 'Endereco'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'C'), 2, false, 10, null, 'numero'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'Complemento'), (SELECT id_campo FROM campo c WHERE nome = 'Endereco'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'C'), 3, false, 60, null, 'complemento'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'Bairro'), (SELECT id_campo FROM campo c WHERE nome = 'Endereco'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'C'), 4, false, 60, null, 'bairro'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'CodigoMunicipio'), (SELECT id_campo FROM campo c WHERE nome = 'Endereco'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'N'), 5, false, 7, 0, 'codigoMunicipio'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'Uf'), (SELECT id_campo FROM campo c WHERE nome = 'Endereco'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'C'), 6, false, 2, null, 'uf'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'Cep'), (SELECT id_campo FROM campo c WHERE nome = 'Endereco'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'N'), 7, false, 8, 0, 'cep'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'Contato'), (SELECT id_campo FROM campo c WHERE nome = 'PrestadorServico'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'c'), 5, false, null, null, 'contato'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'Telefone'), (SELECT id_campo FROM campo c WHERE nome = 'Contato'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'C'), 1, false, 11, null, 'telefone'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'Email'), (SELECT id_campo FROM campo c WHERE nome = 'Contato'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'C'), 2, false, 80, null, 'email'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'TomadorServico'), (SELECT id_campo FROM campo c WHERE nome = 'InfNfse'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'c'), 16, true, null, null, 'tomadorServico'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'IdentificacaoTomador'), (SELECT id_campo FROM campo c WHERE nome = 'TomadorServico'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'c'), 1, false, null, null, 'identificacaoTomador'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'CpfCnpj'), (SELECT id_campo FROM campo c WHERE nome = 'IdentificacaoTomador'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'c'), 1, false, null, null, 'cpfCnpj'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'Cpf'), (SELECT id_campo FROM campo c WHERE nome = 'CpfCnpj'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'C'), 1, false, 11, null, 'cpf'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'Cnpj'), (SELECT id_campo FROM campo c WHERE nome = 'CpfCnpj'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'C'), 2, false, 14, null, 'cnpj'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'InscricaoMunicipal'), (SELECT id_campo FROM campo c WHERE nome = 'IdentificacaoTomador'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'C'), 2, false, 15, null, 'inscricaoMunicipal'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'RazaoSocial'), (SELECT id_campo FROM campo c WHERE nome = 'TomadorServico'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'C'), 2, false, 115, null, 'razaoSocial'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'Endereco'), (SELECT id_campo FROM campo c WHERE nome = 'TomadorServico'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'c'), 3, false, null, null, 'endereco'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'Endereco'), (SELECT id_campo FROM campo c WHERE nome = 'Endereco'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'C'), 1, false, 125, null, 'endereco'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'Numero'), (SELECT id_campo FROM campo c WHERE nome = 'Endereco'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'C'), 2, false, 10, null, 'numero'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'Complemento'), (SELECT id_campo FROM campo c WHERE nome = 'Endereco'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'C'), 3, false, 60, null, 'complemento'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'Bairro'), (SELECT id_campo FROM campo c WHERE nome = 'Endereco'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'C'), 4, false, 60, null, 'bairro'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'CodigoMunicipio'), (SELECT id_campo FROM campo c WHERE nome = 'Endereco'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'N'), 5, false, 7, 0, 'codigoMunicipio'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'Uf'), (SELECT id_campo FROM campo c WHERE nome = 'Endereco'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'C'), 6, false, 2, null, 'uf'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'Cep'), (SELECT id_campo FROM campo c WHERE nome = 'Endereco'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'N'), 7, false, 8, 0, 'cep'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'Contato'), (SELECT id_campo FROM campo c WHERE nome = 'TomadorServico'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'c'), 4, false, null, null, 'contato'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'Telefone'), (SELECT id_campo FROM campo c WHERE nome = 'Contato'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'C'), 1, false, 11, null, 'telefone'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'Email'), (SELECT id_campo FROM campo c WHERE nome = 'Contato'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'C'), 2, false, 80, null, 'email'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'IntermediarioServico'), (SELECT id_campo FROM campo c WHERE nome = 'InfNfse'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'c'), 17, false, null, null, 'intermediarioServico'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'RazaoSocial'), (SELECT id_campo FROM campo c WHERE nome = 'IntermediarioServico'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'C'), 1, true, 115, null, 'razaoSocial'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'CpfCnpj'), (SELECT id_campo FROM campo c WHERE nome = 'IntermediarioServico'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'c'), 2, true, null, null, 'cpfCnpj'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'Cpf'), (SELECT id_campo FROM campo c WHERE nome = 'CpfCnpj'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'C'), 1, false, 11, null, 'cpf'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'Cnpj'), (SELECT id_campo FROM campo c WHERE nome = 'CpfCnpj'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'C'), 2, false, 14, null, 'cnpj'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'InscricaoMunicipal'), (SELECT id_campo FROM campo c WHERE nome = 'IntermediarioServico'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'C'), 3, false, 15, null, 'inscricaoMunicipal'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'OrgaoGerador'), (SELECT id_campo FROM campo c WHERE nome = 'InfNfse'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'c'), 18, true, null, null, 'orgaoGerador'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'CodigoMunicipio'), (SELECT id_campo FROM campo c WHERE nome = 'OrgaoGerador'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'N'), 1, true, 7, 0, 'codigoMunicipio'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'Uf'), (SELECT id_campo FROM campo c WHERE nome = 'OrgaoGerador'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'C'), 2, true, 2, null, 'uf'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'ConstrucaoCivil'), (SELECT id_campo FROM campo c WHERE nome = 'InfNfse'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'c'), 19, false, null, null, 'construcaoCivil'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'CodigoObra'), (SELECT id_campo FROM campo c WHERE nome = 'ConstrucaoCivil'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'C'), 1, true, 15, null, 'codigoObra'),
      (${id_layout}, (SELECT id_campo FROM campo c WHERE nome = 'Art'), (SELECT id_campo FROM campo c WHERE nome = 'ConstrucaoCivil'), (SELECT id_tipo_campo FROM tipo_campo tc WHERE identificador = 'C'), 2, true, 15, null, 'art');
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`
      DELETE FROM campo_layout
      WHERE id_layout = (SELECT id_layout FROM layout l
                          WHERE l.id_tipo_documento = (
                                      SELECT id_tipo_documento
                                      FROM tipo_documento
                                      WHERE sigla = 'NFS-e'
                                    ) AND
                                l.id_municipio = (
                                      SELECT id_municipio
                                      FROM municipio
                                      WHERE codigo_ibge_municipio = 4106902
                                    )
                          )
    `);
  }
}
