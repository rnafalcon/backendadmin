import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateCampo1592490824845 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`
      CREATE TABLE campo (
        id_campo BIGSERIAL PRIMARY KEY,
        nome VARCHAR(60) NOT NULL,
        descricao VARCHAR(100) NOT NULL,
        created TIMESTAMP NOT NULL DEFAULT now(),
        updated TIMESTAMP NOT NULL DEFAULT now()
      )
    `);

    await queryRunner.query(`
      ALTER TABLE campo ADD CONSTRAINT uq_campo_nome UNIQUE (nome);
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.dropTable('campo');
  }
}
