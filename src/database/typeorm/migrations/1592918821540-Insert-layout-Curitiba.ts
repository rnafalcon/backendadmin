import { MigrationInterface, QueryRunner } from 'typeorm';

export class InsertLayoutCuritiba1592918821540 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`
        INSERT INTO layout (id_tipo_documento, id_municipio)
        VALUES (2, (SELECT id_municipio
                    FROM municipio
                    WHERE codigo_ibge_municipio = 4106902));
      `);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`
        DELETE FROM layout
        WHERE
          id_tipo_documento = 2 AND
          id_municipio = (SELECT id_municipio
                          FROM municipio
                          WHERE codigo_ibge_municipio = 4106902);
      `);
  }
}
