import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateTenant1592488253527 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`
      CREATE TABLE tenant (
        id_tenant BIGSERIAL PRIMARY KEY,
        tenant_slug VARCHAR(60) NOT NULL,
        db_name VARCHAR(60) NOT NULL,
        db_host VARCHAR(60) NOT NULL,
        db_username VARCHAR(60) NOT NULL,
        db_password VARCHAR(255) NOT NULL,
        db_port INTEGER NOT NULL,
        id_empresa BIGINT NOT NULL,
        active BOOLEAN NOT NULL DEFAULT TRUE,
        created TIMESTAMP NOT NULL DEFAULT NOW(),
        updated TIMESTAMP NOT NULL DEFAULT NOW()
      )
  `);

    await queryRunner.query(`
      ALTER TABLE tenant
        ADD CONSTRAINT fk_tenant_id_empresa
        FOREIGN KEY (id_empresa) REFERENCES empresa (id_empresa)
        ON DELETE NO ACTION ON UPDATE NO ACTION
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.dropTable('tenant');
  }
}
