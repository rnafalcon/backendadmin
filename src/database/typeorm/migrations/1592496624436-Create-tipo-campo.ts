import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateTipoCampo1592496624436 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`
      CREATE TABLE tipo_campo (
        id_tipo_campo SMALLINT PRIMARY KEY,
        nome VARCHAR(60) NOT NULL,
        identificador VARCHAR(2) NOT NULL,
        created TIMESTAMP NOT NULL DEFAULT now(),
        updated TIMESTAMP NOT NULL DEFAULT now()
      )
    `);

    await queryRunner.query(`
      ALTER TABLE tipo_campo ADD CONSTRAINT uq_tipo_campo_identificador UNIQUE (identificador);
    `);

    await queryRunner.query(`
        INSERT INTO tipo_campo(id_tipo_campo, nome, identificador)
          VALUES
          (1, 'Caractere', 'C'),
          (2, 'Número', 'N'),
          (3, 'Data', 'D'),
          (4, 'Data e Hora', 'DH'),
          (5, 'Complexo', 'c');
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.dropTable('tipo_campo');
  }
}
