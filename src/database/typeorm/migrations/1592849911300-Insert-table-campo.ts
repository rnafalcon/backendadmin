import { MigrationInterface, QueryRunner } from 'typeorm';

export class InsertTableCampo1592849911300 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`
      INSERT INTO campo (nome, descricao)
      VALUES
        ('Nfse', 'Representa a estrutura da Nota Fiscal de Serviços Eletrônica assinada'),
        ('InfNfse', 'Representa os dados informativos da Nota Fiscal de Serviços Eletrônica'),
        ('Numero', 'Número da NFS-e / Número do RPS / Número do endereço'),
        ('CodigoVerificacao', 'Código de verificação do número da nota'),
        ('DataEmissao', 'Data de emissão NFSE'),
        ('IdentificacaoRps', 'Dados de identificação do RPS'),
        ('Serie', 'Número de série do RPS'),
        ('Tipo', 'Código de tipo de RPS'),
        ('DataEmissaoRps', 'Data de emissão do RPS'),
        ('NaturezaOperacao', 'Código de natureza da operação'),
        ('RegimeEspecialTributacao', 'Código de identificação do regime especial de tributação'),
        ('OptanteSimplesNacional', 'Informe se o contribuinte é optante do Simples nacional'),
        ('IncentivadorCultural', 'Informe se o contribuinte é incentivador cultural'),
        ('Competencia', 'Data em que o serviço foi prestado'),
        ('NfseSubstituida', 'Número da NFS-e que será substituída'),
        ('OutrasInformacoes', 'Informações adicionais ao documento'),
        ('Servico', 'Representa dados que compõe o serviço prestado'),
        ('Valores', 'Representa um conjunto de valores que compõe o documento fiscal'),
        ('ValorServicos', 'O valor dos serviços'),
        ('ValorDeducoes', 'O valor das deduções'),
        ('NumeroDeducao', 'Número da dedução gerada pelo módulo de deduções'),
        ('ValorPis', 'Valor do PIS sobre o serviço'),
        ('ValorCofins', 'Valor do COFINS sobre o serviço'),
        ('ValorInss', 'Valor do INSS sobre o serviço'),
        ('ValorIr', 'Valor do IR sobre o serviço'),
        ('ValorCsll', 'Valor do CSLL sobre o serviço'),
        ('IssRetido', 'Identificação se o ISS foi retido pelo tomador ou intermediário'),
        ('ValorIss', 'Valor do ISS sobre o serviço'),
        ('OutrasRetencoes', 'Valor de outras retenções'),
        ('BaseCalculo', 'Valor base usado para apuração do tributo'),
        ('Aliquota', 'Alíquota aplicada sobre base de cálculo para apuração do tributo. Valor percentual. Formato: 0.0000'),
        ('ValorLiquidoNfse', 'Valor líquido da NFS-e considerando valor dos serviços, impostos, retenções e descontos'),
        ('ValorIssRetido', 'Valor do ISS retido'),
        ('DescontoCondicionado', 'Valor do desconto condicionado'),
        ('DescontoIncondicionado', 'Valor do desconto incondicionado'),
        ('ItemListaServico', 'Código de item da lista de serviço'),
        ('CodigoCnae', 'Código CNAE'),
        ('CodigoTributacaoMunicipio', 'Código de Tributação'),
        ('Discriminacao', 'Discriminação do conteúdo da NFS-e'),
        ('CodigoMunicipio', 'Código de identificação do município conforme tabela do IBGE'),
        ('ValorCredito', 'Valor de crédito'),
        ('PrestadorServico', 'Representa dados do prestador do serviço'),
        ('IdentificacaoPrestador', 'Representa dados para identificação do prestador de serviço'),
        ('Cnpj', 'Número de CNPJ'),
        ('InscricaoMunicipal', 'Inscrição Municipal'),
        ('RazaoSocial', 'Razão Social'),
        ('NomeFantasia', 'Nome Fantasia'),
        ('Endereco', 'Representação do endereço'),
        ('Complemento', 'Complemento do endereço'),
        ('Bairro', 'Nome do bairro'),
        ('Uf', 'Sigla do estado'),
        ('Cep', 'CEP da localidade'),
        ('Contato', 'Representa forma de contato com a pessoa (física/jurídica)'),
        ('Telefone', 'Telefone de contato'),
        ('Email', 'Email de contato'),
        ('TomadorServico', 'Representa dados do tomador de serviço'),
        ('IdentificacaoTomador', 'Representa dados do tomador de serviço'),
        ('CpfCnpj', 'Número de CPF ou CNPJ'),
        ('Cpf', 'Número de CPF'),
        ('IntermediarioServico', 'Representa dados para identificação de intermediário do serviço'),
        ('OrgaoGerador', 'Representa dados para identificação de órgão gerador'),
        ('ConstrucaoCivil', 'Representa dados para identificação de construção civil'),
        ('CodigoObra', 'Código de obra'),
        ('Art', 'Código ART');
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query('DELETE FROM campo');
  }
}
