import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddColumnTokenTableTenant1593888917028 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`
      ALTER TABLE tenant ADD COLUMN token_api VARCHAR(255)
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.dropColumn('tenant', 'token_api');
  }
}
