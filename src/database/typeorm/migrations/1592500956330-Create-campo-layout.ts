import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateCampoLayout1592500956330 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`
      CREATE TABLE campo_layout (
        id_campo_layout BIGSERIAL PRIMARY KEY,
        id_layout INTEGER NOT NULL,
        id_campo BIGINT NOT NULL,
        id_campo_pai BIGINT,
        id_tipo_campo SMALLINT NOT NULL,
        ordem SMALLINT NOT NULL,
        obrigatorio BOOLEAN NOT NULL,
        tamanho SMALLINT,
        decimal SMALLINT,
        json_key VARCHAR(60) NOT NULL,
        created TIMESTAMP NOT NULL DEFAULT now(),
        updated TIMESTAMP NOT NULL DEFAULT now()
      )
    `);

    await queryRunner.query(`
      ALTER TABLE campo_layout
        ADD CONSTRAINT fk_campo_layout_id_layout
        FOREIGN KEY (id_layout) REFERENCES layout(id_layout)
        ON DELETE NO ACTION ON UPDATE NO ACTION
    `);

    await queryRunner.query(`
      ALTER TABLE campo_layout
        ADD CONSTRAINT fk_campo_layout_id_campo
        FOREIGN KEY (id_campo) REFERENCES campo(id_campo)
        ON DELETE NO ACTION ON UPDATE NO ACTION
    `);

    await queryRunner.query(`
      ALTER TABLE campo_layout
        ADD CONSTRAINT fk_campo_layout_id_campo_pai
        FOREIGN KEY (id_campo_pai) REFERENCES campo(id_campo)
        ON DELETE NO ACTION ON UPDATE NO ACTION
    `);

    await queryRunner.query(`
      ALTER TABLE campo_layout
        ADD CONSTRAINT fk_campo_layout_id_tipo_campo
        FOREIGN KEY (id_tipo_campo) REFERENCES tipo_campo(id_tipo_campo)
        ON DELETE NO ACTION ON UPDATE NO ACTION
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.dropTable('campo_layout');
  }
}
