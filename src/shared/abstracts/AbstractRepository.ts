import { Repository, FindOneOptions, FindManyOptions, RemoveOptions } from 'typeorm';

abstract class AbstractRepository<T> {
  public repository: Repository<T>;

  public async insert(entity: T): Promise<T> {
    const entityCreated = this.repository.create(entity);

    await this.repository.save(entityCreated);

    return entityCreated;
  }

  public async update(entity: T): Promise<T> {
    return this.repository.save(entity);
  }

  public async delete(entity: T, options?: RemoveOptions): Promise<T> {
    return this.repository.remove(entity, options);
  }

  public async deleteAll(entities: T[], options?: RemoveOptions): Promise<T[]> {
    return this.repository.remove(entities, options);
  }

  public async findById(id: string | number, options?: FindOneOptions<T>): Promise<T | undefined> {
    return this.repository.findOne(String(id), options);
  }

  public async findAll(options?: FindManyOptions<T>): Promise<T[]> {
    return this.repository.find(options);
  }
}

export default AbstractRepository;
