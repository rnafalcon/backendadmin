import { CreateDateColumn, UpdateDateColumn } from 'typeorm';

abstract class AbstractEntity {
  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;
}

export default AbstractEntity;
