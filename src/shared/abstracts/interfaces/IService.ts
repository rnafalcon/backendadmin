export default interface IRepository<T> {
  /**
   * Insere a Entity no banco de dados.
   * @param data Entity
   */
  insert(data: T): Promise<T>;

  /**
   * Atualiza a Entity no banco de dados.
   * @param data Entity
   */
  update(data: T): Promise<T>;

  /**
   * Busca a Entity no banco de dados pelo id.
   * @param data identificador da Entity
   */
  findById(id: string | number): Promise<T | undefined>;
}
