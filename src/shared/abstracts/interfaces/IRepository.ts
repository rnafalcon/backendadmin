import { FindOneOptions, RemoveOptions } from 'typeorm';

export default interface IRepository<T> {
  /**
   * Insere a Entity no banco de dados.
   * @param data Entity
   */
  insert(data: T): Promise<T>;

  /**
   * Atualiza a Entity no banco de dados.
   * @param data Entity
   */
  update(data: T): Promise<T>;

  /**
   * Deleta uma entidade do banco de dados.
   * @param entity Entity que será deletada
   * @param options opções para o delete
   */
  delete(entity: T, options?: RemoveOptions): Promise<T>;

  /**
   * Deleta um arry de entidades do baco de dados.
   * @param entities Entitdades que serão deletadas
   * @param options opções para o deleteAll
   */
  deleteAll(entities: T[], options?: RemoveOptions): Promise<T[]>;

  /**
   * Busca a Entity no banco de dados pelo id.
   * @param id identificador da Entity
   * @param options opções para o find
   */
  findById(id: string | number, options?: FindOneOptions<T>): Promise<T | undefined>;
}
