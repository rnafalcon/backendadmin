//bom para converter o Xml para o JSON
//não achei um jeito certo de trasnformar as tags
import xml2js, { Builder, processors } from 'xml2js';
import camelcaseKeys from 'camelcase-keys';
const { firstCharLowerCase, parseNumbers } = processors;

function firstCharUpperCase(str: string) {
  console.log('CHAR UPPER CASE');
  return str.charAt(0).toLocaleUpperCase() + str.slice(1);
}

const json = {
  token_api: 'string', //obrigatorio
  numero: 0,
  codigoVerificacao: 'string',
  dataEmissao: '2020-06-24T18:01:27.914Z',
  dataEmissaoRps: '2020-06-24T18:01:27.914Z',
  identificacaoRps: {
    //essa informação é gerado por quem?
    numero: 0,
    serie: 'string',
    tipo: 0,
  },
  naturezaOperacao: 1,
  regimeEspecialTributacao: 0, //pode ser informacao da empresa?
  optanteSimplesNacional: 0, //pode ser informacao da empresa?
  incentivadorCultural: 0, //pode ser informacao da empresa?
  competencia: '2020-06-24T18:01:27.914Z', //data prestação serviço?
  nfseSubstituida: 0,
  outrasInformacoes: 0,
  tomador: {
    cpfCnpj: 'string',
    inscricaoMunicipal: 'string',
    razaoSocial: 'string',
    endereco: {
      rua: 'string',
      numero: 'string',
      complemento: 'string',
      bairro: 'string',
      codigoMunicipio: 123,
      uf: 'string',
      cep: 'string',
    },
    contato: {
      telefone: 'string',
      email: 'string',
    },
  },
  servico: {
    valores: {
      valorServicos: 0.0,
      valorDeducoes: 0.0,
      numeroDeducao: 123456,
      valorPis: 0.0,
      valorCofins: 0.0,
      valorInss: 0.0,
      valorIr: 0.0,
      valorCsll: 0.0,
      issRetido: 0.0,
      valorIss: 0.0,
      outrasRetencoes: 0.0,
      baseCalculo: 0.0,
      aliquota: 0.0,
      valorLiquidoNfse: 0.0,
      valorIssRetido: 0.0,
      descontoIncondicionado: 0.0,
      descontoCondicionado: 0.0,
    },
    codigoServico: 'string', //(ItemListaServico)
    codigoCnae: 123456,
    codigoTributacaoMunicipio: 'string',
    discriminacao: 'string',
    codigoMunicipio: 123,
  },
  valorCredito: 0.0, //o que é isso exatamente?
  intermediarioServico: {
    razaoSocial: 'string',
    cpfCnpj: 'string',
    inscricaoMunicipal: 'string',
  },
  orgaoGerador: {
    codigoMunicipio: 123456,
    uf: 'string',
  },
  construcaoCivil: {
    codigoObra: 'string',
    art: 'string',
  },
};

const jsonInteiro = {
  nfse: {
    infNfse: {
      $: {
        id: 'http://foo.com',
      },
      ...json,
    },
  },
};

const jsonReturn = {
  infNfse: {
    attr: { id: 'asdasdasd' },
    numero: 12,
    codigoVerificacao: '4F1Y040V',
    dataEmissao: '2020-01-21T10:11:39.213',
    dataEmissaoRps: '0001-01-01T00:00:00',
    naturezaOperacao: 1,
    regimeEspecialTributacao: 0,
    optanteSimplesNacional: 2,
    incentivadorCultural: 2,
    competencia: '0001-01-01T00:00:00',
    nfseSubstituida: 0,
    servico: {
      valores: {
        valorServicos: 12063.35,
        valorDeducoes: 0,
        valorPis: 0.0,
        valorCofins: 0.423,
        valorInss: 0.123,
        valorIr: 0,
        valorCsll: 0,
        issRetido: 2,
        valorIss: 241.26,
        valorIssRetido: 0,
        outrasRetencoes: 0,
        baseCalculo: 12063.35,
        aliquota: 0.02,
        valorLiquidoNfse: 12063.35,
        descontoIncondicionado: 0,
        descontoCondicionado: 0,
      },
      itemListaServico: 107,
      codigoCnae: 0,
      discriminacao: 'SERVIÇOS PRESTADOS',
      codigoMunicipio: 0,
    },
    valorCredito: 0,
    prestadorServico: {
      identificacaoPrestador: { cnpj: 9590766000148, inscricaoMunicipal: 8293066 },
      nomeFantasia: 'RNA TECNOLOGIA DA INFORMACAO EIRELI',
      endereco: {
        endereco: 'R.VISCONDE DE NACAR',
        numero: '1510 CJ 60',
        complemento: '',
        bairro: 'CENTRO',
        codigoMunicipio: 4106902,
        uf: 'PR',
        cep: 0,
      },
    },
    tomadorServico: {
      identificacaoTomador: { cpfCnpj: { cnpj: 31000498000116 } },
      razaoSocial: 'FH SERVIÇOS DE TECNOLOGIA LTDA',
      endereco: {
        endereco: 'RUA JOINVILLE',
        numero: 2334,
        complemento: 'SALA 04',
        bairro: 'SAO PEDRO',
        codigoMunicipio: 4125506,
        uf: 'PR',
        cep: 83005550,
      },
      contato: { email: 'FRANCIELLY.RAMOS@FH.COM.BR' },
    },
  },
};

var xml = `
<Nfse>
<InfNfse Id="asdasdasd">
  <Numero>12</Numero>
  <CodigoVerificacao>4F1Y040V</CodigoVerificacao>
  <DataEmissao>2020-01-21T10:11:39.213</DataEmissao>
  <DataEmissaoRps>0001-01-01T00:00:00</DataEmissaoRps>
  <NaturezaOperacao>1</NaturezaOperacao>
  <RegimeEspecialTributacao>0</RegimeEspecialTributacao>
  <OptanteSimplesNacional>2</OptanteSimplesNacional>
  <IncentivadorCultural>2</IncentivadorCultural>
  <Competencia>0001-01-01T00:00:00</Competencia>
  <NfseSubstituida>0</NfseSubstituida>
  <Servico>
    <Valores>
      <ValorServicos>12063.35</ValorServicos>
      <ValorDeducoes>0.00</ValorDeducoes>
      <ValorPis>0.00</ValorPis>
      <ValorCofins>0.00</ValorCofins>
      <ValorInss>0.00</ValorInss>
      <ValorIr>0.00</ValorIr>
      <ValorCsll>0.00</ValorCsll>
      <IssRetido>2</IssRetido>
      <ValorIss>241.26</ValorIss>
      <ValorIssRetido>0.00</ValorIssRetido>
      <OutrasRetencoes>0.00</OutrasRetencoes>
      <BaseCalculo>12063.35</BaseCalculo>
      <Aliquota>0.02</Aliquota>
      <ValorLiquidoNfse>12063.35</ValorLiquidoNfse>
      <DescontoIncondicionado>0.00</DescontoIncondicionado>
      <DescontoCondicionado>0.00</DescontoCondicionado>
    </Valores>
    <ItemListaServico>0107</ItemListaServico>
    <CodigoCnae>0</CodigoCnae>
    <Discriminacao>SERVIÇOS PRESTADOS</Discriminacao>
    <CodigoMunicipio>0</CodigoMunicipio>
  </Servico>
  <ValorCredito>0</ValorCredito>
  <PrestadorServico>
    <IdentificacaoPrestador>
      <Cnpj>09590766000148</Cnpj>
      <InscricaoMunicipal>08293066</InscricaoMunicipal>
    </IdentificacaoPrestador>
    <NomeFantasia>RNA TECNOLOGIA DA INFORMACAO EIRELI</NomeFantasia>
    <Endereco>
      <Endereco>R.VISCONDE DE NACAR</Endereco>
      <Numero>1510 CJ 60</Numero>
      <Complemento />
      <Bairro>CENTRO</Bairro>
      <CodigoMunicipio>4106902</CodigoMunicipio>
      <Uf>PR</Uf>
      <Cep>0</Cep>
    </Endereco>
  </PrestadorServico>
  <TomadorServico>
    <IdentificacaoTomador>
      <CpfCnpj>
        <Cnpj>31000498000116</Cnpj>
      </CpfCnpj>
    </IdentificacaoTomador>
    <RazaoSocial>FH SERVIÇOS DE TECNOLOGIA LTDA</RazaoSocial>
    <Endereco>
      <Endereco>RUA JOINVILLE</Endereco>
      <Numero>2334</Numero>
      <Complemento>SALA 04</Complemento>
      <Bairro>SAO PEDRO</Bairro>
      <CodigoMunicipio>4125506</CodigoMunicipio>
      <Uf>PR</Uf>
      <Cep>83005550</Cep>
    </Endereco>
    <Contato>
      <Email>FRANCIELLY.RAMOS@FH.COM.BR</Email>
    </Contato>
  </TomadorServico>
</InfNfse>
</Nfse>
`;

// xml2js
//   .parseStringPromise(xml, {
//     attrkey: 'attr',
//     explicitRoot: false,
//     explicitArray: false,
//     preserveChildrenOrder: true,
//     tagNameProcessors: [firstCharLowerCase],
//     attrNameProcessors: [firstCharLowerCase],
//     valueProcessors: [parseNumbers],
//   })
//   .then(function (result: any) {
//     console.log(JSON.stringify(result));
//     console.log('Done');
//   })
//   .catch(function (err: any) {
//     // Failed
//   });

var builder = new Builder({
  attrkey: 'attr',
  explicitRoot: false,
  rootName: 'Nfse',
  headless: true,
  preserveChildrenOrder: true,
});

const formattedJson = camelcaseKeys(jsonReturn, {
  deep: true,
  pascalCase: true,
  exclude: ['attr'],
});

var xmlk = builder.buildObject(formattedJson);
console.log(xmlk);
