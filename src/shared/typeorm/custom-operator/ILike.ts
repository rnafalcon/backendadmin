import { FindOperator } from 'typeorm';
import FindOperatorWithExtras from '../overrides/FindOperatorWithExtras';

export function ILike<T>(value: T | FindOperator<T>): FindOperatorWithExtras<T> {
  return new FindOperatorWithExtras('ilike', value);
}
