import { Request, Response, NextFunction } from 'express';
import { verify } from 'jsonwebtoken';

import ApiError from '@shared/errors/ApiError';
import authConfig from '@config/auth';

interface TokenPayload {
  userId: number;
  email: string;
  tenantSlug: string;
  iat: number;
  exp: number;
}

export default function auth(req: Request, res: Response, next: NextFunction): void {
  const authHeader = req.headers.authorization;

  if (!authHeader) {
    throw new ApiError('Token não foi informado', 401);
  }

  try {
    const [, token] = authHeader.split(' ');

    const decoded = verify(token, authConfig.secret) as TokenPayload;

    req.loggedUserId = decoded.userId;

    return next();
  } catch (error) {
    throw new ApiError('Token inválido', 401);
  }
}
