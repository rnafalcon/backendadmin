import { Request, Response, NextFunction } from 'express';
import ApiError from '@shared/errors/ApiError';
import log from '@config/log4js';

export default async function errorHandler(
  err: Error,
  req: Request,
  res: Response,
  _: NextFunction
): Promise<Response> {
  log.error(err.message);

  return err instanceof ApiError
    ? res.status(err.statusCode).json({
        message: err.message,
      })
    : res.status(500).json({
        message: 'Houve erro no servidor',
      });
}
