import adminRoutes from '@admin/routes';
import { Router } from 'express';

const routes = Router();

routes.use('/admin', adminRoutes);

export default routes;
