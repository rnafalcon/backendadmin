import { container } from 'tsyringe';
import requireDirectory from 'require-directory';

// const requireConfig = {
//   extensions: ['js', 'ts'],
//   exclude: /spec.ts$/,
// };

// const admin = requireDirectory(module, '../modules/admin', requireConfig);
// const shared = requireDirectory(module, '../modules/shared', requireConfig);

// const modules = [admin, shared];

// //@ts-ignore
// function isArrayInArray(arr, item) {
//   var item_as_string = JSON.stringify(item);

//   //@ts-ignore
//   var contains = arr.some(function (ele) {
//     return JSON.stringify(ele) === item_as_string;
//   });
//   return contains;
// }

// modules.forEach((m) => {
//   if (m.services) {
//     //@ts-ignore
//     const { impl, interfaces } = m.services;
//     const implArray = Object.entries(impl);
//     const interfacesArray = Object.entries(interfaces);

//     implArray.forEach((implementation) => {
//       console.log(interfacesArray);
//       const nameImpl = implementation[0];
//       const classImpl = implementation[1];

//       const nameInterface = String.prototype.concat('I', nameImpl.replace('Impl', ''));
//       console.log(interfacesArray);
//     });
//     // container.registerSingleton<ITenantService>('TenantService', TenantServiceImpl);
//   }
// });

import ITenantService from '@admin/services/interfaces/ITenantService';
import TenantServiceImpl from '@admin/services/impl/TenantServiceImpl';

import ITenantRepository from '@admin/repositories/interfaces/ITenantRepository';
import TenantRepositoryImpl from '@admin/repositories/impl/TenantRepositoryImpl';

import IEmpresaService from '@admin/services/interfaces/IEmpresaService';
import EmpresaServiceImpl from '@admin/services/impl/EmpresaServiceImpl';

import IEmpresaRepository from '@admin/repositories/interfaces/IEmpresaRepository';
import EmpresaRepositoryImpl from '@admin/repositories/impl/EmpresaRepositoryImpl';

import IMunicipioService from '@admin/services/interfaces/IMunicipioService';
import MunicipioServiceImpl from '@admin/services/impl/MunicipioServiceImpl';

import IMunicipioRepository from '@admin/repositories/interfaces/IMunicipioRepository';
import MunicipioRepositoryImpl from '@admin/repositories/impl/MunicipioRepositoryImpl';

container.registerSingleton<ITenantService>('TenantService', TenantServiceImpl);
container.registerSingleton<ITenantRepository>('TenantRepository', TenantRepositoryImpl);

container.registerSingleton<IEmpresaService>('EmpresaService', EmpresaServiceImpl);
container.registerSingleton<IEmpresaRepository>('EmpresaRepository', EmpresaRepositoryImpl);

container.registerSingleton<IMunicipioService>('MunicipioService', MunicipioServiceImpl);
container.registerSingleton<IMunicipioRepository>('MunicipioRepository', MunicipioRepositoryImpl);
