describe('TenantServiceImpl', () => {
  it ('somar corretamente', async () => {
    const a = 2 + 5;

    expect(a).toEqual(7);
  });

  it ('somar incorretamente', async () => {
    const a = 1 + 5;

    expect(a).not.toEqual(7);
  });
})
