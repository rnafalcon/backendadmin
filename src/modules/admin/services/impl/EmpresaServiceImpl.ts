import { injectable, inject } from 'tsyringe';
import axios from 'axios';

import ApiError from '@shared/errors/ApiError';
import IEmpresaService from '@admin/services/interfaces/IEmpresaService';
import IEmpresaRepository from '@admin/repositories/interfaces/IEmpresaRepository';
import IMunicipioService from '@admin/services/interfaces/IMunicipioService';

import Empresa from '@admin/models/Empresa';

@injectable()
class EmpresaServiceImpl implements IEmpresaService {
  constructor(
    @inject('EmpresaRepository')
    private empresaRepository: IEmpresaRepository,

    @inject('MunicipioService')
    private municipioService: IMunicipioService
  ) {}

  public async insertByCnpj(cnpj: string): Promise<Empresa> {
    const { data } = await axios.get(`https://www.receitaws.com.br/v1/cnpj/${cnpj}`);

    const {
      nome: razaoSocial,
      fantasia: nomeFantasia,
      natureza_juridica: naturezaJuridica,
      municipio,
      uf,
      abertura,
      ...rest
    } = data;

    const municipioObject = await this.municipioService.findByNomeAndSiglaEstado(municipio, uf);
    const aberturaSplit = abertura.split('/');

    const empresa = await this.empresaRepository.insert({
      ...rest,
      abertura: new Date(aberturaSplit[2], aberturaSplit[1], aberturaSplit[0]),
      municipio: municipioObject,
      razaoSocial,
      nomeFantasia,
      naturezaJuridica,
    });

    return empresa;
  }

  public async update(data: Empresa): Promise<Empresa> {
    const { idEmpresa } = data;
    const empresa = await this.empresaRepository.findById(idEmpresa);

    if (!empresa) {
      throw new ApiError('Não existe empresa cadastrada com esse código');
    }

    try {
      const empresaUpdated = await this.empresaRepository.update(data);

      return empresaUpdated;
    } catch (error) {
      throw new ApiError('Erro ao atualizar empresa');
    }
  }

  public async findById(idEmpresa: string): Promise<Empresa | undefined> {
    try {
      return this.empresaRepository.findById(idEmpresa, {
        relations: ['municipio'],
      });
    } catch {
      throw new ApiError('Erro ao buscar empresa');
    }
  }

  public async findByCnpj(cnpj: string): Promise<Empresa | undefined> {
    return this.empresaRepository.findByCnpj(cnpj);
  }
}

export default EmpresaServiceImpl;
