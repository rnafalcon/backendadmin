import { injectable, inject } from 'tsyringe';

import Municipio from '@admin/models/Municipio';
import IMunicipioService from '@admin/services/interfaces/IMunicipioService';
import IMunicipioRepository from '@admin/repositories/interfaces/IMunicipioRepository';

@injectable()
class MunicipioServiceImpl implements IMunicipioService {
  constructor(
    @inject('MunicipioRepository')
    private municipioRepository: IMunicipioRepository
  ) {}

  public async findByNomeAndSiglaEstado(
    nome: string,
    sigla: string
  ): Promise<Municipio | undefined> {
    return this.municipioRepository.findByNomeAndSiglaEstado(nome, sigla);
  }
}

export default MunicipioServiceImpl;
