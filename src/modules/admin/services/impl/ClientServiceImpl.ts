import crypto from 'crypto';
import { inject, injectable } from 'tsyringe';
import { format, isValid } from '@fnando/cnpj';
import { plainToClass } from 'class-transformer';
import ApiError from '@shared/errors/ApiError';

import Client from '@admin/models/Client';
import Tenant from '@admin/models/Tenant';
import IClientService from '@admin/services/interfaces/IClientService';
import IEmpresaService from '@admin/services/interfaces/IEmpresaService';
import ITenantService from '@admin/services/interfaces/ITenantService';

@injectable()
class ClientServiceImpl implements IClientService {
  constructor(
    @inject('EmpresaService')
    private empresaService: IEmpresaService,

    @inject('TenantService')
    private tenantService: ITenantService
  ) {}

  public async insert(client: Client): Promise<Client> {
    const { cnpj, tenantSlug, ...rest } = client;

    const tenantExists = await this.tenantService.findByTenantSlug(tenantSlug);

    if (tenantExists) {
      throw new ApiError('Tenant já existe');
    }

    if (!cnpj) {
      throw new ApiError('Parâmetro obrigatório não informado: cnpj.');
    }

    if (!isValid(format(cnpj))) {
      throw new ApiError('CNPJ inválido');
    }

    if (await this.empresaService.findByCnpj(format(cnpj))) {
      throw new ApiError('CNPJ informado já está cadastrado na base de dados.');
    }

    const empresa = await this.empresaService.insertByCnpj(cnpj);
    const tokenApi = this.createTokenApi(empresa.cnpj, tenantSlug);

    await this.tenantService.insert(
      plainToClass(Tenant, { ...rest, tenantSlug, empresa, tokenApi })
    );

    return client;
  }

  private createTokenApi(cnpj: string, tenantSlug: string): string {
    const cipher = crypto.createCipher('aes256', cnpj);
    cipher.update(tenantSlug);
    return cipher.final('hex');
  }
}

export default ClientServiceImpl;
