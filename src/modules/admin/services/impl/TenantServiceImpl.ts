import Tenant from '@admin/models/Tenant';
import ITenantRepository from '@admin/repositories/interfaces/ITenantRepository';
import ITenantService from '@admin/services/interfaces/ITenantService';
import { inject, injectable } from 'tsyringe';

@injectable()
class TenantServiceImpl implements ITenantService {
  constructor(
    @inject('TenantRepository')
    private tenantRepository: ITenantRepository
  ) {}

  public async insert(tenant: Tenant): Promise<Tenant> {
    return this.tenantRepository.insert(tenant);
  }

  public async findByTenantSlug(tenantSlug: string): Promise<Tenant | undefined> {
    return this.tenantRepository.findByTenantSlug(tenantSlug);
  }

  public async findByToken(token: string): Promise<Tenant | undefined> {
    return this.tenantRepository.findByToken(token);
  }
}

export default TenantServiceImpl;
