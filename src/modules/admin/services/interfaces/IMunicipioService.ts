import Municipio from '../../models/Municipio';

export default interface IMunicipioService {
  findByNomeAndSiglaEstado(nome: string, codigoUf: string): Promise<Municipio | undefined>;
}
