import Tenant from '@admin/models/Tenant';

export default interface ITenantService {
  insert(tenant: Tenant): Promise<Tenant>;

  /**
   * Método responsável por buscar o Tenant conforme o slug.
   * @param tenantSlug o slug do tenant.
   */
  findByTenantSlug(tenantSlug: string): Promise<Tenant | undefined>;

  /**
   * Método responsável por buscar o Tenant conforme o token.
   * @param token token da api para o tenant.
   */
  findByToken(token: string): Promise<Tenant | undefined>;
}
