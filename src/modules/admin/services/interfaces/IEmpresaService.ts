import Empresa from '@admin/models/Empresa';

export default interface IEmpresaService {
  update(data: Empresa): Promise<Empresa>;
  insertByCnpj(cnpj: string): Promise<Empresa>;
  findById(idEmpresa: string): Promise<Empresa | undefined>;
  findByCnpj(cnpj: string): Promise<Empresa | undefined>;
}
