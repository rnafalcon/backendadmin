import { Request, Response } from 'express';
import { container } from 'tsyringe';

import ApiError from '@shared/errors/ApiError';
import EmpresaServiceImpl from '@admin/services/impl/EmpresaServiceImpl';

class EmpresaController {
  async update(req: Request, res: Response): Promise<Response> {
    try {
      const data = req.body;
      const empresaService = container.resolve(EmpresaServiceImpl);
      const empresa = await empresaService.update(data);

      return res.status(200).json(empresa);
    } catch (error) {
      throw new ApiError(error.message);
    }
  }

  async findById(req: Request, res: Response): Promise<Response> {
    try {
      const { idEmpresa } = req.params;
      const empresaService = container.resolve(EmpresaServiceImpl);
      const empresa = await empresaService.findById(idEmpresa);

      return res.status(200).json(empresa);
    } catch (error) {
      throw new ApiError(error.message);
    }
  }
}

export default new EmpresaController();
