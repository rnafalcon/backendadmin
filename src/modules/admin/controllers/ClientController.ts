import { Request, Response } from 'express';
import { container } from 'tsyringe';

import ApiError from '@shared/errors/ApiError';
import ClientServiceImpl from '@admin/services/impl/ClientServiceImpl';

class ClientController {
  async insert(req: Request, res: Response): Promise<Response> {
    try {
      const data = req.body;
      const clientService = container.resolve(ClientServiceImpl);
      const client = await clientService.insert(data);

      return res.status(201).send(client);
    } catch (error) {
      throw new ApiError(error.message);
    }
  }
}

export default new ClientController();
