import TenantServiceImpl from '@admin/services/impl/TenantServiceImpl';
import { Request, Response } from 'express';
import { container } from 'tsyringe';

class TenantController {
  async findByTenantSlug(req: Request, res: Response): Promise<Response> {
    const { tenantSlug } = req.params;
    const tenantService = container.resolve(TenantServiceImpl);
    const tenant = await tenantService.findByTenantSlug(tenantSlug);

    return res.status(200).json(tenant);
  }

  async findByToken(req: Request, res: Response): Promise<Response> {
    const { token } = req.params;
    const tenantService = container.resolve(TenantServiceImpl);
    const tenant = await tenantService.findByToken(token);

    return res.status(200).json(tenant);
  }
}

export default new TenantController();
