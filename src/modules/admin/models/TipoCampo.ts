import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';
import AbstractEntity from '@shared/abstracts/AbstractEntity';

@Entity()
class TipoCampo extends AbstractEntity {
  @PrimaryGeneratedColumn({ name: 'id_tipo_campo' })
  idTipoCampo: number;

  @Column()
  nome: string;

  @Column()
  identificador: string;
}

export default TipoCampo;
