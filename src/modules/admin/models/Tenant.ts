import AbstractEntity from '@shared/abstracts/AbstractEntity';
import { Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import Empresa from './Empresa';

@Entity()
class Tenant extends AbstractEntity {
  @PrimaryGeneratedColumn({ name: 'id_tenant' })
  idTenant: number;

  @Column({ name: 'tenant_slug' })
  tenantSlug: string;

  @Column({ name: 'db_name' })
  dbName: string;

  @Column({ name: 'db_host' })
  dbHost: string;

  @Column({ name: 'db_username' })
  dbUsername: string;

  @Column({ name: 'db_password' })
  dbPassword: string;

  @Column({ name: 'db_port' })
  dbPort: number;

  @OneToOne((_) => Empresa)
  @JoinColumn({ name: 'id_empresa' })
  empresa: Empresa;

  @Column({ name: 'token_api' })
  tokenApi: string;

  @Column({ default: true })
  active: boolean;
}

export default Tenant;
