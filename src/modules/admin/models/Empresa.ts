import AbstractEntity from '@shared/abstracts/AbstractEntity';
import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import Municipio from './Municipio';

@Entity()
class Empresa extends AbstractEntity {
  @PrimaryGeneratedColumn({ name: 'id_empresa' })
  idEmpresa: number;

  @Column({ name: 'razao_social' })
  razaoSocial: string;

  @Column()
  telefone: string;

  @Column()
  email: string;

  @Column()
  situacao: string;

  @Column()
  bairro: string;

  @Column()
  logradouro: string;

  @Column()
  complemento: string;

  @Column()
  numero: string;

  @Column()
  cep: string;

  @ManyToOne((_) => Municipio)
  @JoinColumn({ name: 'id_municipio' })
  municipio: Municipio;

  @Column()
  porte: string;

  @Column()
  abertura: Date;

  @Column({ name: 'natureza_juridica' })
  naturezaJuridica: string;

  @Column({ name: 'nome_fantasia' })
  nomeFantasia: string;

  @Column({ unique: true })
  cnpj: string;

  @Column()
  tipo: string;

  @Column({ default: true })
  active: boolean;
}

export default Empresa;
