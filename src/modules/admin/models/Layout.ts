import { Entity, PrimaryGeneratedColumn, JoinColumn, ManyToOne, OneToOne, Index } from 'typeorm';
import AbstractEntity from '@shared/abstracts/AbstractEntity';

import TipoDocumento from './TipoDocumento';
import Municipio from './Municipio';

@Entity()
@Index(['tipoDocumento', 'municipio'], { unique: true })
class Layout extends AbstractEntity {
  @PrimaryGeneratedColumn({ name: 'id_layout ' })
  idLayout: number;

  @OneToOne((_) => TipoDocumento)
  @JoinColumn({ name: 'id_tipo_documento' })
  tipoDocumento: TipoDocumento;

  @ManyToOne((_) => Municipio)
  @JoinColumn({ name: 'id_municipio' })
  municipio: Municipio;
}

export default Layout;
