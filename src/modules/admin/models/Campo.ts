import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';
import AbstractEntity from '@shared/abstracts/AbstractEntity';

@Entity()
class Campo extends AbstractEntity {
  @PrimaryGeneratedColumn({ name: 'id_campo' })
  idCampo: number;

  @Column()
  nome: string;

  @Column()
  descricao: string;
}

export default Campo;
