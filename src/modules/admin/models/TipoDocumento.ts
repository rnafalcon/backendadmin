import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';
import AbstractEntity from '@shared/abstracts/AbstractEntity';

@Entity()
class TipoDocumento extends AbstractEntity {
  @PrimaryGeneratedColumn({ name: 'id_tipo_documento' })
  idTipoDocumento: number;

  @Column()
  nome: string;

  @Column()
  sigla: string;
}

export default TipoDocumento;
