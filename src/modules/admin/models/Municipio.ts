import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from 'typeorm';
import AbstractEntity from '@shared/abstracts/AbstractEntity';

import Estado from './Estado';

@Entity()
class Municipio extends AbstractEntity {
  @PrimaryGeneratedColumn({ name: 'id_municipio' })
  idMunicipio: number;

  @Column()
  nome: string;

  @Column({ name: 'codigo_ibge_municipio' })
  codigoIbgeMunicipio: number;

  @ManyToOne((_) => Estado)
  @JoinColumn({ name: 'id_estado' })
  estado: Estado;
}

export default Municipio;
