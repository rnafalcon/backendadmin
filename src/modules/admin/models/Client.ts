class Client {
  tenantSlug: string;
  dbName: string;
  dbHost: string;
  dbUsername: string;
  dbPassword: string;
  dbPort: number;
  cnpj: string;
}

export default Client;
