import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';
import AbstractEntity from '@shared/abstracts/AbstractEntity';

@Entity()
class Estado extends AbstractEntity {
  @PrimaryGeneratedColumn({ name: 'id_estado' })
  idEstado: number;

  @Column()
  nome: string;

  @Column()
  sigla: string;

  @Column({ name: 'codigo_uf' })
  codigoUf: number;
}

export default Estado;
