import { Entity, PrimaryGeneratedColumn, Column, OneToOne, ManyToOne, JoinColumn } from 'typeorm';
import AbstractEntity from '@shared/abstracts/AbstractEntity';

import Campo from './Campo';
import Layout from './Layout';
import TipoCampo from './TipoCampo';

@Entity()
class CampoLayout extends AbstractEntity {
  @PrimaryGeneratedColumn({ name: 'id_campo_layout' })
  idCampoLayout: number;

  @Column()
  ordem: number;

  @Column()
  obrigatorio: boolean;

  @Column()
  tamanho: number;

  @Column()
  decimal: number;

  @ManyToOne((_) => Layout)
  @JoinColumn({ name: 'id_layout' })
  layout: Layout;

  @OneToOne((_) => Campo)
  @JoinColumn({ name: 'id_campo' })
  campo: Campo;

  @OneToOne((_) => TipoCampo)
  @JoinColumn({ name: 'id_tipo_campo' })
  tipoCampo: TipoCampo;
}

export default CampoLayout;
