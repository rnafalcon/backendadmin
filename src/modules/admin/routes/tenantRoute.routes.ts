import TenantController from '@admin/controllers/TenantController';
import { Router } from 'express';

const tenantRoute = Router();

tenantRoute.get('/:tenantSlug', TenantController.findByTenantSlug);
tenantRoute.get('/find-by-token/:token', TenantController.findByToken);

export default tenantRoute;
