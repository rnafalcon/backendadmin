import { Router } from 'express';

import ClientController from '@admin/controllers/ClientController';

const clientRoute = Router();

clientRoute.post('/', ClientController.insert);

export default clientRoute;
