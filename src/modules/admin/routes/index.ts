import { Router } from 'express';

import clientRoute from './clientRoute.routes';
import empresaRoute from './empresaRoute.routes';
import tenantRoute from './tenantRoute.routes';

const routes = Router();

routes.use('/client', clientRoute);
routes.use('/empresa', empresaRoute);
routes.use('/tenant', tenantRoute);

export default routes;
