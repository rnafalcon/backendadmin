import { Router } from 'express';

import EmpresaController from '@admin/controllers/EmpresaController';

const empresaRoute = Router();

empresaRoute.put('/:idEmpresa', EmpresaController.update);
empresaRoute.get('/:idEmpresa', EmpresaController.findById);

export default empresaRoute;
