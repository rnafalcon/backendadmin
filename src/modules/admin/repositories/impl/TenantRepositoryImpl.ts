import Tenant from '@admin/models/Tenant';
import ITenantRepository from '@admin/repositories/interfaces/ITenantRepository';
import AbstractRepository from '@shared/abstracts/AbstractRepository';
import { getRepository } from 'typeorm';

class TenantRepositoryImpl extends AbstractRepository<Tenant> implements ITenantRepository {
  constructor() {
    super();
    this.repository = getRepository(Tenant);
  }

  public async findByTenantSlug(tenantSlug: string): Promise<Tenant | undefined> {
    return this.repository
      .createQueryBuilder('tenant')
      .innerJoinAndSelect('tenant.empresa', 'empresa')
      .innerJoinAndSelect('empresa.municipio', 'municipio')
      .innerJoinAndSelect('municipio.estado', 'estado')
      .where('tenant.tenantSlug = :tenantSlug', { tenantSlug })
      .getOne();
  }

  public async findByToken(token: string): Promise<Tenant | undefined> {
    return this.repository.findOne({
      select: ['idTenant', 'tenantSlug', 'tokenApi'],
      where: { tokenApi: token },
    });
  }
}

export default TenantRepositoryImpl;
