import { getRepository } from 'typeorm';
import AbstractRepository from '@shared/abstracts/AbstractRepository';
import Municipio from '@admin/models/Municipio';
import IMunicipioRepository from '@admin/repositories/interfaces/IMunicipioRepository';

class MunicipioRepositoryImpl extends AbstractRepository<Municipio>
  implements IMunicipioRepository {
  constructor() {
    super();
    this.repository = getRepository(Municipio);
  }

  public async findByNomeAndSiglaEstado(
    nome: string,
    sigla: string
  ): Promise<Municipio | undefined> {
    return this.repository
      .createQueryBuilder('municipio')
      .innerJoin('municipio.estado', 'estado')
      .where('municipio.nome ILIKE :nome', { nome })
      .andWhere('estado.sigla = :sigla', { sigla })
      .getOne();
  }
}

export default MunicipioRepositoryImpl;
