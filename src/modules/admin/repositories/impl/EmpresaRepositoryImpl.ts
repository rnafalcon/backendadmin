import { getRepository } from 'typeorm';
import AbstractRepository from '@shared/abstracts/AbstractRepository';
import IEmpresaRepository from '@admin/repositories/interfaces/IEmpresaRepository';
import Empresa from '@admin/models/Empresa';

class EmpresaRepositoryImpl extends AbstractRepository<Empresa> implements IEmpresaRepository {
  constructor() {
    super();
    this.repository = getRepository(Empresa);
  }

  public async findByCnpj(cnpj: string): Promise<Empresa | undefined> {
    return this.repository.findOne({ where: { cnpj } });
  }
}

export default EmpresaRepositoryImpl;
