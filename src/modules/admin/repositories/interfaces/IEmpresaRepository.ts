import IRepository from '@shared/abstracts/interfaces/IRepository';
import Empresa from '@admin/models/Empresa';

export default interface IEmpresaRepository extends IRepository<Empresa> {
  /**
   * Método responsável por buscar no banco de dados a Empresa conforme o CNPJ.
   * @param cnpj CNPJ da empresa.
   */
  findByCnpj(cnpj: string): Promise<Empresa | undefined>;
}
