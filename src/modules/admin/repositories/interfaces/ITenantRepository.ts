import Tenant from '@admin/models/Tenant';
import IRepository from '@shared/abstracts/interfaces/IRepository';

export default interface ITenantRepository extends IRepository<Tenant> {
  /**
   * Método responsável por buscar no banco de dados o Tenant conforme o slug.
   * @param tenantSlug o slug do tenant.
   */
  findByTenantSlug(tenantSlug: string): Promise<Tenant | undefined>;

  /**
   * Método responsável por buscar no banco de dados o Tenant conforme o token.
   * @param token token da api para o tenant.
   */
  findByToken(token: string): Promise<Tenant | undefined>;
}
