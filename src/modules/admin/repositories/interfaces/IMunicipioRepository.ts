import IRepository from '@shared/abstracts/interfaces/IRepository';
import Municipio from '@admin/models/Municipio';

export default interface IMunicipioRepository extends IRepository<Municipio> {
  /**
   * Método responsável por buscar no banco de dados o Município conforme o nome.
   * @param nome o nome do Município.
   */
  findByNomeAndSiglaEstado(nome: string, sigla: string): Promise<Municipio | undefined>;
}
